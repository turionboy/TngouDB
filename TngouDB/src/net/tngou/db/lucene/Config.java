package net.tngou.db.lucene;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Config {

	private static Logger log = LoggerFactory.getLogger(LuceneManage.class);
	private static Config  CONFIG= null;
	
	/**
	 * 单例方法
	 * @return
	 */
	public final static Config getInstance (){
		
		
		if(CONFIG!=null) return CONFIG;
		Config conf = new Config();
		Configurations configs = new Configurations();
		// Read data from this file
		File propertiesFile = new File("config.properties");
		try {
			PropertiesConfiguration config = configs.properties(propertiesFile);
			Map<String,  Object> cp_props = new HashMap<String, Object>();
			
			Iterator<String> iterable = config.getKeys();
			while (iterable.hasNext()) 
			{
				String skey=iterable.next();
				cp_props.put(skey, config.getProperty(skey));
			}
		
			BeanUtils.populate(conf, cp_props);
		} catch (ConfigurationException | IllegalAccessException | InvocationTargetException e) {
			log.error("找不到配置文件{}" ,propertiesFile.getPath());
			return null;
//			e.printStackTrace();
		}
		
		return conf;
		
	}
	
	private Config()
	{
		
	}
	
	
	private String path; //数据存储地址
	private int size; //大小
	private String table;//默认表名
	private String locale;//语言
	private int port;//端口
	private int run;//运行状态
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getRun() {
		return run;
	}

	public void setRun(int run) {
		this.run = run;
	}
	
	
	
	
}
